# Joomla CLI
A command line interface for the development and management of Joomla! websites.

## Requirements
`node`
`npm`

## Installation
`npm install -g joomla-cli-2`

## Usage
run `joomla -h` for options

## Todo
* Add cache for login
