const nightmare = require('../custom-nightmare')
const upload = require('nightmare-upload')
const path = require('path')
const out = require('cli-output')
const nmJoomla = require('../utils/nightmare-joomla')
const plugins = nmJoomla.plugins

upload(nightmare)

const basePath = process.cwd()

/**
 * Normalize path if not absolute
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
function normalizePath (p) {
  return path.isAbsolute(p) ? p : path.resolve(basePath, p)
}

function install (opts) {
  const nm = nightmare({
    openDevTools: false,
    show: opts.show,
    waitTimeout: 999999
  })

  const src = normalizePath(opts.source)

  nm
    .goto(`${opts.url}/administrator/index.php?option=com_installer&view=install`)
    .use(plugins.login(opts.adminUser, opts.adminPass))
    .wait('input[name="install_package"]')
    .use(plugins.installExtension(src))
    .end(() => {
      out.success('Extension installed')
    })
    .catch(function (error) {
      out.error('Installation failed: ', error)
    })
}

module.exports = install
