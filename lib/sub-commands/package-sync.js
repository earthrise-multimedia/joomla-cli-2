const path = require('path')
const glob = require('glob')
const fs = require('fs-extra')
const includes = require('lodash/includes')
const indexOf = require('lodash/indexOf')
const isArray = require('lodash/isArray')
const isString = require('lodash/isString')
const parser = require('rapidx2j')
const out = require('cli-output')
const chokidar = require('chokidar')
const shelljs = require('shelljs')

const basePath = process.cwd()

/**
 * Normalize path if not absolute
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
function normalizePath (p) {
  return path.isAbsolute(p) ? p : path.resolve(basePath, p)
}

function sync (opts) {
  const src = normalizePath(opts.source)
  const dest = normalizePath(opts.dest)
  const packages = glob.sync(`${src}/packages/+(com*|mod*|plg*|tpl*)`)

  let pkginfo = getSyncPaths(packages, src, dest)

  pkginfo.syncpaths.src.forEach((filesrc, i) => {
    const filedest = pkginfo.syncpaths.dest[i]
    handleEvent('add', filesrc, filedest)
  })

  out.success('packages synced')

  if (opts.watch) {
    out.success('watching src for changes')

    chokidar.watch(src, {
      ignored: /(^|[\/\\])\../,
      ignoreInitial: true,
      awaitWriteFinish: true,
      usePolling: true
    }).on('all', (event, path) => {
      const pattern = `^(${pkginfo.syncpaths.src.join('|')})(.*)`
      const re = new RegExp(pattern)
      const matches = path.match(re)

      if (pkginfo.configs[path]) {
        pkginfo = getSyncPaths(packages, src, dest)
      }

      if (matches) {
        const index = indexOf(pkginfo.syncpaths.src, matches[1])
        const filesrc = `${matches[1]}${matches[2] ? matches[2] : ''}`
        const filedest = `${pkginfo.syncpaths.dest[index]}${matches[2] ? matches[2] : ''}`

        console.log(`${filesrc} => ${filedest}`)

        handleEvent(event, filesrc, filedest)
      }
    })
  }
}

function getSyncPaths(packages, src, dest) {
  let configs = []
  let syncpaths = { src: [], dest: [] }

  packages.forEach(pkg => {
    if (includes(pkg, '.zip')) return

    const type = path.basename(pkg).slice(0, 3)
    const extdirname = path.basename(pkg)
    const extname = extdirname.replace(`${type}_`, '')
    const configfile = `${pkg}/${(type === 'tpl' ? 'templateDetails.xml' : `${extname}.xml`)}`

    if (!fs.existsSync(configfile)) return

    const config = fs.readFileSync(configfile, 'utf8')
    configs.push({ [configfile]: extdirname })

    const xml = parser.parse(config)
    const pkgsyncpaths = parsePaths(xml, extdirname, src, dest)

    syncpaths.src = syncpaths.src.concat(pkgsyncpaths.src)
    syncpaths.dest = syncpaths.dest.concat(pkgsyncpaths.dest)
  })

  return { configs, syncpaths }
}

function parsePaths (config, extdirname, src, dest) {
  const type = `${config['@type']}s`
  let syncpaths = { src: [], dest: [] }

  switch (type) {
    case 'components': {
      let extname = extdirname.replace('com_', '')

      if (config.files) {
        let srcdir = config.files['@folder']
        let srcc = `${src}/packages/${extdirname}/${srcdir}`
        let target = `${dest}/${type}/${extdirname}`
        syncpaths.src.push(srcc)
        syncpaths.dest.push(target)
      }

      if (config.administration) {
        if (config.administration.files) {
          let srcdir = config.administration.files['@folder']
          let srcc = `${src}/packages/${extdirname}/${srcdir}`
          let target = `${dest}/administrator/${type}/${extdirname}`
          syncpaths.src.push(srcc)
          syncpaths.dest.push(target)
        }

        if (config.administration.languages) {
          let srcdir = config.administration.languages['@folder']
          let languages = !isArray(config.languages.language)
            ? [config.languages.language]
            : config.languages.language

          languages.forEach(lang => {
            let filename = path.basename(lang.keyValue)
            let srcc = `${src}/packages/${extdirname}/${srcdir}/${lang.keyValue}`
            let target = `${dest}/administrator/language/${lang['@tag']}/${filename}`
            syncpaths.src.push(srcc)
            syncpaths.dest.push(target)
          })
        }
      }

      if (config.languages) {
        let srcdir = config.languages['@folder']
        let languages = !isArray(config.languages.language)
          ? [config.languages.language]
          : config.languages.language

        languages.forEach(lang => {
          let filename = path.basename(lang.keyValue)
          let srcc = `${src}/packages/${extdirname}/${srcdir}/${lang.keyValue}`
          let target = `${dest}/language/${lang['@tag']}/${filename}`
          syncpaths.src.push(srcc)
          syncpaths.dest.push(target)
        })
      }

      if (config.scriptfile) {
        let filename = config.scriptfile
        let srcc = `${src}/packages/${extdirname}/${filename}`
        let target = `${dest}/administrator/${type}/${extdirname}/${filename}`
        syncpaths.src.push(srcc)
        syncpaths.dest.push(target)
      }

      // sync config file
      let filename = `${extname}.xml`
      let srcc = `${src}/packages/${extdirname}/${filename}`
      let target = `${dest}/administrator/${type}/${extdirname}/${filename}`
      syncpaths.src.push(srcc)
      syncpaths.dest.push(target)

      break
    }

    case 'plugins': {
      const group = `${config['@group']}`
      const plgdirname = extdirname.replace('plg_', '')

      if (config.files) {
        if (config.files.folder) {
          config.files.folder.forEach(folder => {
            const srcc = `${src}/packages/${extdirname}/${folder}`
            const target = `${dest}/${type}/${group}/${plgdirname}/${folder}`
            syncpaths.src.push(srcc)
            syncpaths.dest.push(target)
          })
        }

        if (config.files.filename) {
          const filenames = !isArray(config.files.filename)
          ? [config.files.filename]
          : config.files.filename

          filenames.forEach(file => {
            const filename = getFileName(file)
            const srcc = `${src}/packages/${extdirname}/${filename}`
            const target = `${dest}/${type}/${group}/${plgdirname}/${filename}`
            syncpaths.src.push(srcc)
            syncpaths.dest.push(target)
          })
        }
      }

      if (config.scriptfile) {
        const srcc = `${src}/packages/${extdirname}/${config.scriptfile}`
        const target = `${dest}/${type}/${group}/${plgdirname}/${config.scriptfile}`
        syncpaths.src.push(srcc)
        syncpaths.dest.push(target)
      }

      // sync config file
      let filename = `${plgdirname}.xml`
      let srcc = `${src}/packages/${extdirname}/${filename}`
      let target = `${dest}/${type}/${group}/${plgdirname}/${filename}`
      syncpaths.src.push(srcc)
      syncpaths.dest.push(target)

      break
    }

    case 'templates': {
      const tpldirname = extdirname.replace('tpl_', '')

      if (config.files) {
        if (config.files.folder) {
          config.files.folder.forEach(folder => {
            const srcc = `${src}/packages/${extdirname}/${folder}`
            const target = `${dest}/templates/${tpldirname}/${folder}`
            syncpaths.src.push(srcc)
            syncpaths.dest.push(target)
          })
        }

        if (config.files.file) {
          config.files.file.forEach(file => {
            const srcc = `${src}/packages/${extdirname}/${file}`
            const target = `${dest}/templates/${tpldirname}/${file}`
            syncpaths.src.push(srcc)
            syncpaths.dest.push(target)
          })
        }
      }

      if (config.scriptfile) {
        const srcc = `${src}/packages/${extdirname}/${config.scriptfile}`
        const target = `${dest}/templates/${tpldirname}/${config.scriptfile}`
        syncpaths.src.push(srcc)
        syncpaths.dest.push(target)
      }

      break
    }
  }

  if (config.media) {
    const srcdir = config.media['@folder']
    const destdir = config.media['@destination']
    const srcc = `${src}/packages/${extdirname}/${srcdir}`
    const target = `${dest}/media/${destdir}`
    syncpaths.src.push(srcc)
    syncpaths.dest.push(target)
  }

  return syncpaths
}

function getFileName(item) {
  return !isString(item) ? item.keyValue : item
}

function handleEvent (event, src, dest) {
  fs.removeSync(dest)

  switch (event) {
    case 'add':
    case 'addDir':
    case 'change': {
      shelljs.exec(`cp -rp ${src} ${dest}`)
      break
    }
  }
}

module.exports = sync
