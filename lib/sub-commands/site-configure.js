const nightmare = require('nightmare')
const out = require('cli-output')

/**
 * Checks for error alerts
 * if any are found
 * @return {[type]} [description]
 */
nightmare.action('checkForAlert', function (done) {
  this.evaluate_now(function () {
    let alert = document.querySelector('.alert.alert-error div')
    if (alert) throw new Error(alert.innerText)
  }, done)
})

/**
 * Starts the configuration process
 * @param  {[type]} opts [description]
 * @return {[type]}      [description]
 */
function configure (opts) {
  const nm = nightmare({
    openDevTools: false,
    show: opts.show
  })

  nm.goto(`${opts.url}/installation/index.php`)
    // check that site is not already configured
    .evaluate(() => {
      // check if we get to the homepage
      let pageHeader = document.querySelector('.page-header')
      let successPage = document.querySelector('.alert-success')

      if (pageHeader) {
        throw new Error('Site already configured.')
      }

      // check if the sites been configured but the installation folder has not been deleted
      if (successPage) {
        throw new Error(
          'Site already configured. ' +
          'Additionally, It appears the installation folder was not removed. ' +
          'Please remove manually.'
        )
      }
    })
    .type('form#adminForm [name="jform[site_name]"]', opts.name)
    .type('form#adminForm [name="jform[admin_email]"]', opts.adminEmail)
    .type('form#adminForm [name="jform[admin_user]"]', opts.adminUser)
    .type('form#adminForm [name="jform[admin_password]"]', opts.adminPass)
    .type('form#adminForm [name="jform[admin_password2]"]', opts.adminPass)
    .click('a.btn.btn-primary')
    .wait('#database.active')
    .type('form#adminForm [name="jform[db_host]"]', false)
    .type('form#adminForm [name="jform[db_host]"]', opts.dbHost)
    .type('form#adminForm [name="jform[db_name]"]', opts.dbName)
    .type('form#adminForm [name="jform[db_user]"]', opts.dbUser)
    .type('form#adminForm [name="jform[db_pass]"]', opts.dbPass)
    .click('a.btn.btn-primary')
    .wait(10000)
    .checkForAlert()
    .click('a.btn.btn-primary')
    .wait('.alert.alert-success')
    .checkForAlert()
    .end()
    .then(() => {
      out.success('Joomla! site successfully installed')
      process.exit(0)
    })
    .catch(function (error) {
      out.error('Configuration failed: ', error)
      process.exit(0)
    })
}

module.exports = configure
