const nightmare = require('../custom-nightmare')
const out = require('cli-output')
const nmJoomla = require('../utils/nightmare-joomla')
const plugins = nmJoomla.plugins

function getGroupID (name) {
  if (!isNaN(name)) return name

  const groups = {
    'Public': 1,
    'Guest': 9,
    'Manager': 6,
    'Administrator': 7,
    'Registered': 2,
    'Author': 3,
    'Editor': 4,
    'Publisher': 5,
    'Super Users': 8
  }

  return groups[name]
}

/**
 * Starts the user creation process
 * @param  {[type]} opts [description]
 * @return {[type]}      [description]
 */
function create (opts) {
  const nm = nightmare({
    openDevTools: false,
    show: opts.show
  })

  const groupid = getGroupID(opts.userGroup)

  nm
    .goto(`${opts.url}/administrator/index.php?option=com_users&view=users`)
    .use(plugins.login(opts.adminUser, opts.adminPass))
    .wait('body[class*="com_users"]')
    .click('button[onclick="Joomla.submitbutton(\'user.add\');"]')
    .wait('body[class*="layout-edit"]')
    .type('#jform_name', opts.userName)
    .type('#jform_username', opts.userLogin)
    .type('#jform_password', opts.userPass)
    .type('#jform_password2', opts.userPass)
    .type('#jform_email', opts.userEmail)
    .click('a[href="#groups"]')
    .check(`input[value="${groupid}"]`)
    .click('button[onclick="Joomla.submitbutton(\'user.save\');"]')
    .wait(2000)
    .end(() => {
      out.success('User successfully created')
    })
    .catch(function (error) {
      out.error('User creation failed: ', error)
    })
}

module.exports = create
