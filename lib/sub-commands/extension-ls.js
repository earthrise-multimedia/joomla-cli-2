const nightmare = require('nightmare')
const out = require('cli-output')
const nmJoomla = require('../utils/nightmare-joomla')
const plugins = nmJoomla.plugins

function ls (opts) {
  const nm = nightmare({
    openDevTools: false,
    show: opts.show
  })

  nm
    .goto(`${opts.url}/administrator/index.php?option=com_installer&view=manage`)
    .use(plugins.login(opts.adminUser, opts.adminPass))
    .wait('#manageList')
    .use(plugins.listTable('#manageList', ['name', 'type', 'id'], opts.keyword))
    .catch(function (error) {
      out.error('Listing failed: ', error)
    })
}

module.exports = ls
