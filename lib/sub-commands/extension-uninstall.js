/* global Joomla */
const nightmare = require('nightmare')
const out = require('cli-output')
const nmJoomla = require('../utils/nightmare-joomla')
const plugins = nmJoomla.plugins

function uninstall (opts) {
  const nm = nightmare({
    openDevTools: false,
    show: opts.show
  })

  nm
    .on('page', function (type = 'confirm', message, response) {
      const match = 'Are you sure you want to uninstall? Confirming will permanently delete the selected item(s)!'

      if (message === match) {
        return 1
      }

      return response
    })
    .goto(`${opts.url}/administrator/index.php?option=com_installer&view=manage`)
    .use(plugins.login(opts.adminUser, opts.adminPass))
    .wait('#manageList')
    .use(plugins.expandTable('#manageList'))
    .click(`input[value="${opts.id}"]`)
    .evaluate(() => {
      return Joomla.submitbutton('manage.remove')
    })
    .then(() => {
      out.success('Uninstall successful.')
    })
    .catch(function (error) {
      out.error('Installation failed: ', error)
    })
}

module.exports = uninstall
