const path = require('path')
const glob = require('glob')
const fs = require('fs-extra')
const out = require('cli-output')
const zip = require('zip-folder')
const deasync = require('deasync')

const basePath = process.cwd()

/**
 * Normalize path if not absolute
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
function normalizePath (p) {
  return path.isAbsolute(p) ? p : path.resolve(basePath, p)
}

function archive (opts) {
  const src = normalizePath(opts.source)
  const dest = normalizePath(opts.dest)
  const name = path.basename(glob.sync(`${src}/*.xml`)[0], '.xml')

  // clean the dir
  fs.emptyDirSync(dest)

  // copy over the source
  fs.copySync(src, path.join(dest, name))

  // remove OS files
  glob.sync(path.join(dest, '**', '+(*DS_Store)')).forEach(fs.removeSync)

  // make the package dir
  fs.mkdirsSync(path.join(dest, name, 'packages'))

  // zip extensions and move to packages folder
  glob.sync(`${path.join(dest, name)}/+(com*|mod*|plg*|tpl*)/`).forEach(ext => {
    const extName = path.basename(ext)
    const zipSrc = ext
    const zipDest = path.join(dest, name, 'packages', `${extName}.zip`)
    let done = false

    // zip the extension
    // shell.exec(`cd ${path.join(dest, name)} && zip -r ${extName}.zip ${extName} `)

    zip(zipSrc, zipDest, (err) => {
      if (err) {
        out.error(`Archiving failed: ${err}`)
        process.exit()
      }

      fs.move(zipSrc, zipDest, () => {
        fs.removeSync(zipSrc)
        done = true
      })
    })

    deasync.loopWhile(() => !done)
  })

  // move already existing zips
  glob.sync(`${path.join(dest, name)}/*.zip`).forEach(ext => {
    const extName = path.basename(ext)
    const zipSrc = ext
    const zipDest = path.join(dest, name, 'packages', extName)
    let done = false

    fs.move(zipSrc, zipDest, () => {
      fs.removeSync(ext)
      done = true
    })

    deasync.loopWhile(() => !done)
  })

  // zip package
  // shell.exec(`cd ${dest} && zip -r ${name}.zip ${name}`)
  zip(path.join(dest, name), path.join(dest, `${name}.zip`), (err) => {
    if (err) {
      out.error(`Archiving failed: ${err}`)
      process.exit()
    }

    fs.removeSync(path.join(dest, name))
    out.success('Package successfully archived')
  })

  // remove package dir
  // fs.removeSync(path.join(dest, name))

  // out.success('Package successfully archived')
}

module.exports = archive
