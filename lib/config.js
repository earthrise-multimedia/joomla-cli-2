const path = require('path')

let config = {
  basePath: process.cwd(),
  pkgBasePath: path.dirname(module.id)
}

module.exports = config
