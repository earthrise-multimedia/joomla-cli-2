const commander = require('commander')
const Command = commander.Command
const Option = commander.Option
const camelCase = require('lodash/camelCase')

Command.prototype.option = function (flags, description, fn, defaultValue) {
  const self = this
  const option = new Option(flags, description)
  const oname = option.name()
  const name = camelCase(oname)

  // default as 3rd arg
  if (typeof fn !== 'function') {
    if (fn instanceof RegExp) {
      const regex = fn
      fn = (val, def) => {
        const m = regex.exec(val)
        return m ? m[0] : def
      }
    } else {
      defaultValue = fn
      fn = null
    }
  }

  // set default value of the option
  option.defaultValue = defaultValue

  // preassign default value only for --no-*, [optional], or <required>
  if (option.bool === false || option.optional || option.required) {
    // when --no-* we make sure default is true
    if (option.bool === false) defaultValue = true
    // preassign only if we have a default
    if (undefined !== defaultValue) self[name] = defaultValue
  }

  // register the option
  this.options.push(option)

  // when it's passed assign the value
  // and conditionally invoke the callback
  this.on(oname, val => {
    // coercion
    if (val !== null && fn) {
      val = fn(val, undefined === self[name]
      ? defaultValue
      : self[name])
    }

    // unassigned or bool
    if (typeof self[name] === 'boolean' || typeof self[name] === 'undefined') {
      // if no value, bool true, and we have a default, then use it!
      if (val === null) {
        self[name] = option.bool
          ? defaultValue || true
          : false
      } else {
        self[name] = val
      }
    } else if (val !== null) {
      // reassign
      self[name] = val
    }
  })

  return this
}

Command.prototype.parseOptions = function (argv) {
  const args = []
  const len = argv.length
  let literal
  let option
  let arg

  // if no argv, then check if some required option
  if (len === 0) {
    for (let j = 0, optionLen = this.options.length; j < optionLen; ++j) {
      option = this.options[j]
      if (option.required && option.defaultValue === undefined) {
        return this.optionMissingArgument(option)
      }
    }
  }

  const unknownOptions = []

  // parse options
  for (let i = 0; i < len; ++i) {
    arg = argv[i]

    // literal args after --
    if (arg === '--') {
      literal = true
      continue
    }

    if (literal) {
      args.push(arg)
      continue
    }

    // find matching Option
    option = this.optionFor(arg)

    // option is defined
    if (option) {
      // requires arg
      if (option.required) {
        arg = argv[++i]
        if (arg === null) return this.optionMissingArgument(option)
        this.emit(option.name(), arg)
      // optional arg
      } else if (option.optional) {
        arg = argv[i + 1]

        if (arg === null || arg === undefined || (arg[0] === '-' && arg !== '-')) {
          arg = null
        } else {
          ++i
        }
        this.emit(option.name(), arg)
      // bool
      } else {
        this.emit(option.name())
      }
      continue
    }

    // looks like an option
    if (arg.length > 1 && arg[0] === '-') {
      unknownOptions.push(arg)

      // If the next argument looks like it might be
      // an argument for this option, we pass it on.
      // If it isn't, then it'll simply be ignored
      if (argv[i + 1] && argv[i + 1][0] !== '-') {
        unknownOptions.push(argv[++i])
      }
      continue
    }

    // arg
    args.push(arg)
  }

  return { args, unknown: unknownOptions }
}

Command.prototype.helpInformation = function () {
  let desc = []
  if (this._description) {
    desc = [ `  ${this._description}`, '' ]
  }

  let cmdName = this._name
  if (this._alias) {
    cmdName = `${cmdName}|${this._alias}`
  }
  const usage = [
    '',
    `  Usage: ${cmdName.replace('-', ' ')} ${this.usage()}`,
    ''
  ]

  let cmds = []
  const commandHelp = this.commandHelp()
  if (commandHelp) cmds = [commandHelp]

  const options = [
    '  Options:',
    '',
    `${this.optionHelp().replace(/^/gm, '    ')}`,
    '',
    ''
  ]

  return usage
    .concat(cmds)
    .concat(desc)
    .concat(options)
    .join('\n')
}

module.exports = new Command()
