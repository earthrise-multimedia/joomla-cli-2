const path = require('path')
const fs = require('fs-extra')
const config = require('./config')

const version = () => {
  const pkgPath = path.join(config.pkgBasePath, '../package.json')
  return fs.readJsonSync(pkgPath).version
}

module.exports = version
