const filter = require('lodash/filter')
const includes = require('lodash/includes')
const pick = require('lodash/pick')
const Table = require('easy-table')
const out = require('cli-output')

// Actions
function getAlerts (done) {
  return this.evaluate_now(function () {
    const els = document.getElementsByClassName('alert')
    let alerts = {}

    Array.prototype.slice.call(els).forEach(function (el) {
      const typeEl = el.querySelector('.alert-heading')
      const msgEl = el.querySelector('.alert-message')
      const type = typeEl ? typeEl.innerText.trim().toLowerCase() : null
      const msg = msgEl ? msgEl.innerText.trim() : null

      if (!msg) return

      if (!alerts[type]) alerts[type] = []

      alerts[type].push(msg)
    })

    return alerts
  }, done)
}

function waitForUnload (done) {
  var maxWaitTime = this.options.executionTimeout // or whatever you like

  this.evaluate_now(function () {
    return new Promise(function(resolve) {
      window.onbeforeunload = function() {
        resolve()
      }
    })
  }, done)
  // It's OK not to clean up this timer; once `done` has been called,
  // later calls to it are ignored.
  setTimeout(function() {
    done({message: 'waitForUnload timeout'})
  }, maxWaitTime)
}

// function processAlerts (alerts) {
//   if (alerts.error || alerts.warning) {
//     Object.keys(alerts).forEach(function (type) {
//       alerts[type].forEach(function (msg) {
//         out.log(`Joomla ${type}: ${msg}`)
//       })
//     })

//     throw new Error('See the Joomla! alerts above')
//   }
// }

// Plugins
function login (user, pass) {
  return function (nightmare) {
    nightmare
      .type('input[name="username"]', user)
      .type('input[name="passwd"]', pass)
      .click('.btn.btn-primary')
  }
}

function installExtension (path) {
  return function (nightmare) {
    const inputSelector = 'input[name="install_package"]'

    nightmare
      .wait(inputSelector)
      .upload(inputSelector, path)
      .click('#installbutton_package')
      .wait(20000)
      .wait(() => {
        const loading = document.querySelector('#loading')

        if (loading && loading.style.display === 'none') {
          return true
        }
      })
      .wait('.alert.alert-success')
  }
}

function expandTable (selector) {
  return function (nightmare) {
    nightmare
      .select('#list_limit', '0')
      .wait('#list_limit option[value="0"][selected="selected"]')
      .wait(selector)
  }
}

function getTable (selector) {
  return function (nightmare) {
    nightmare
      .use(expandTable(selector))
      .evaluate(selector => {
        const table = document.querySelector(selector)
        const th = table.querySelectorAll('thead th')
        const rows = table.querySelectorAll('tbody tr')
        let out = []

        Array.prototype.slice.call(rows).forEach((row, rowi) => {
          const cells = row.querySelectorAll('td')
          const data = {}

          Array.prototype.slice.call(cells).forEach((cell, celli) => {
            data['index'] = rowi
            data[th[celli].innerText.trim().toLowerCase() || celli] = cell.innerText.trim().toLowerCase() || celli
          })

          out.push(data)
        })

        return out
      }, selector)
  }
}

function listTable (table, selected, keyword) {
  return function (nightmare) {
    nightmare
      .use(getTable(table))
      .then(res => {
        res = res.map(obj => pick(obj, selected))

        if (keyword) {
          res = filter(res, obj => {
            const string = Object.keys(obj).map(key => obj[key]).join()
            return includes(string, keyword.trim().toLowerCase())
          })
        }

        console.log(Table.print(res))
        process.exit(0)
      })
  }
}

const actions = {
  getAlerts,
  waitForUnload
}

const plugins = {
  login,
  installExtension,
  expandTable,
  getTable,
  listTable
}


module.exports = { actions, plugins }
