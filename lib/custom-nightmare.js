const nightmare = require('nightmare')
const nmJoomla = require('./utils/nightmare-joomla')

Object.keys(nmJoomla.actions).forEach((name) => {
  const action = nmJoomla.actions[name]
  nightmare.action(name, action)
})



module.exports = nightmare
