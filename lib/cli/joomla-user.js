const program = require('../program')
const version = require('../version')

/**
 * Add program commands below
 */

program
  .version(version())
  .command('create', 'create a new user')
  .parse(process.argv)
