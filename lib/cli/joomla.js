const program = require('../program')
const version = require('../version')
// bin

/**
 * Add program commands below
 */

program
  .version(version())
  .command('site [command]', 'configure and checkin a joomla site')
  .command('package [command]', 'archive and symlink packages')
  .command('extension [command]', 'install, and uninstall extensions')
  .command('user [command]', 'list, create, modify, and delete users')
  .parse(process.argv)
