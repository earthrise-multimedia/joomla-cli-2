/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const sync = require('../sub-commands/package-sync')

/**
 * Add program commands below
 */

program
  .version(version())
  .option('-s, --source <path>', 'source directory of the package', '.')
  .option('-d, --dest <path>', 'Joomla! install root directory')
  .option('--watch [bool]', false)
  .parse(process.argv)

sync(program)
