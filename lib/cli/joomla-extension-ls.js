/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const ls = require('../sub-commands/extension-ls')

/**
 * Add program commands below
 */

program
  .version(version())
  .option('--url <url>', 'the web url of the site', 'http://localhost')
  .option('--admin_user <admin_user>', 'the username for your Super User account', 'admin')
  .option('--admin_pass <admin_pass>', 'the password for your Super User account', 'admin')
  .option('-k, --keyword [keyword]', 'optional keyword to filter packages by')
  .option('--show [bool]', 'display the browser window while running')
  .parse(process.argv)

ls(program)
