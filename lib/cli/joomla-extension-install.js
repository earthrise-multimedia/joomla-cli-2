/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const install = require('../sub-commands/extension-install')

/**
 * Add program commands below
 */

program
  .version(version())
  .option('--url <url>', 'the web url of the site', 'http://localhost')
  .option('--admin_user <admin_user>', 'the username for your Super User account', 'admin')
  .option('--admin_pass <admin_pass>', 'the password for your Super User account', 'admin')
  .option('-s, --source <path>', 'source directory of the package')
  .option('--show [bool]', 'display the browser window while running')
  .parse(process.argv)

install(program)
