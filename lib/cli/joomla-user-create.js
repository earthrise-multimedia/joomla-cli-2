/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const configure = require('../sub-commands/user-create')

/**
 * Add program commands below
 */

program
  .version(version())
  .usage('[options]')
  .option('--url <url>', 'the web url of the site', 'http://localhost')
  .option('--admin_user <admin_user>', 'the username for your Super User account', 'admin')
  .option('--admin_pass <admin_pass>', 'the password for your Super User account', 'admin')
  .option('--user_name <user_name>', 'the name for the new user', 'user')
  .option('--user_login <user_login>', 'the login name for the new user', 'user')
  .option('--user_pass <user_pass>', 'the password for the new user', 'user')
  .option('--user_email <user_email>', 'the email for the new user', 'user@user.com')
  .option('--user_group <user_group>', 'the group to assign to the new user', 'Manager')
  .option('--show [bool]', 'display the browser window while running')
  .parse(process.argv)

configure(program)
