const program = require('../program')
const version = require('../version')

/**
 * Add program commands below
 */

program
  .version(version())
  .command('ls', 'list installed Joomla! extensions')
  .command('install', 'install a Joomla! extension')
  .command('uninstall', 'uninstall a Joomla! extension')
  .parse(process.argv)
