/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const configure = require('../sub-commands/site-configure')

/**
 * Add program commands below
 */

program
  .version(version())
  .usage('[options]')
  .option('--url <url>', 'the web url of the site', 'http://localhost')
  .option('--name <name>', 'the name of your Joomla! site', 'joomla')
  .option('--admin_email <admin_email>', 'the email address of the website Super User', 'admin@admin.com')
  .option('--admin_user <admin_user>', 'the username for your Super User account', 'admin')
  .option('--admin_pass <admin_pass>', 'the password for your Super User account', 'admin')
  .option('--db_host <db_host>', 'this is usually "localhost"', 'localhost')
  .option('--db_name <db_name>', 'some hosts allow only a certain DB name per site. Use table prefix in this case for distinct Joomla! sites', 'joomla')
  .option('--db_user <db_user>', 'for site security using a password for the database account is mandatory', 'root')
  .option('--db_pass <db_pass>', 'either something as "root" or a username given by the host', 'root')
  .option('--show [bool]', 'display the browser window while running')
  .parse(process.argv)

configure(program)
