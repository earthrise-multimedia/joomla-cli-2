const program = require('../program')
const version = require('../version')

/**
 * Add program commands below
 */

program
  .version(version())
  .command('configure', 'configure a joomla site')
  .parse(process.argv)
