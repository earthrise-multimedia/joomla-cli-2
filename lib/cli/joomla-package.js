const program = require('../program')
const version = require('../version')

/**
 * Add program commands below
 */

program
  .version(version())
  .command('archive', 'archive a Joomla! package')
  .command('sync', 'sync a package to a Joomla! install dir')
  .parse(process.argv)
