/* eslint max-len: 0 */
const program = require('../program')
const version = require('../version')
const archive = require('../sub-commands/package-archive')

/**
 * Add program commands below
 */

program
  .version(version())
  .option('-s, --source <path>', 'source directory of the package', '.')
  .option('-d, --dest <path>', 'destination directory of the package')
  .parse(process.argv)

archive(program)
