const glob = require('glob')
const path = require('path')
const fs = require('fs-extra')
const includes = require('lodash/includes')

const basePath = process.cwd()
const configPath = path.join(basePath, 'package.json')

let config = fs.readJsonSync(configPath)

glob(path.join(basePath, 'lib', 'cli', '**', '*.js'), function (err, files) {
  if (err) throw new Error(err)

  config.bin = files.reduce((obj, file) => {
    const basename = path.basename(file)
    const binPath = path.join(basePath, 'bin', basename)

    fs.outputFileSync(
      binPath,
      `#!/usr/bin/env node\n\nrequire('../lib/cli/${basename}');\n`
    )

    if (includes(fs.readFileSync(file, 'utf8'), '// bin')) {
      obj[basename.replace('.js', '')] = `bin/${basename}`
    }

    return obj
  }, {})

  fs.writeJsonSync(configPath, config)
})
